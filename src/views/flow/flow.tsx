import { defineComponent, onMounted } from "vue";
import LogicFlow from '@logicflow/core';
import '@logicflow/core/dist/style/index.css';

import { InsertNodeInPolyline } from '@logicflow/extension';
import '@logicflow/extension/lib/style/index.css'
LogicFlow.use(InsertNodeInPolyline);
const flow = defineComponent({
  name:"flow",
  setup(){
    const data = {
      // 节点
      nodes: [
        {
          id: 50,
          type: 'rect',
          x: 100,
          y: 150,
          text: '你好',
        },
        {
          id: 21,
          type: 'circle',
          x: 300,
          y: 150,
        },
      ],
      // 边
      edges: [
        {
          type: 'polyline',
          sourceNodeId: 50,
          targetNodeId: 21,
        },
      ],
    };
    let lf:LogicFlow;
    onMounted(()=>{
       lf = new LogicFlow({
        container: document.querySelector('#flow-wallpaper'),
        stopScrollGraph: true,
        stopZoomGraph: true,
        height:800,
        grid: {
          type: 'dot',
          size: 20,
          config:{
            color:"red"
          }
        },
      });
      lf.render(data);
    })
    return () =><div class="w-full">
      <div id="flow-wallpaper"></div>
    </div>
  }
})
export default flow
