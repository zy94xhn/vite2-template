import { baseConfig } from "@config/base.config";
import { useSysStore } from "@store/sys";
import { computed, defineComponent, KeepAlive, Transition } from "vue";
import { darkTheme, NConfigProvider } from "naive-ui";
import { RouterView } from "vue-router";

const layout = defineComponent({
  components: { KeepAlive, Transition },
  setup() {
    const sysStore = useSysStore();
    const isDark = computed(() => {
      return sysStore.theme === "dark" ? darkTheme : undefined;
    });
    const sys = useSysStore();
    const name = baseConfig.componetTrans || "fade";
    return () => (
      <NConfigProvider theme={isDark.value}>
        <div class=" w-screen flex flex-col">
          <div class="flex">
            <div class="w-auto">
              <z-slider></z-slider>
            </div>
            <div class="flex-1 main-content h-screen">
              <z-header />
              <z-tags class="hidden md4:flex"></z-tags>
              <RouterView class="p-2 flex-1 h-auto overflow-y-scroll relative">
                {{
                  default: ({ Component, route }) => {
                    return (
                      <Transition name={name} mode="out-in">
                        <KeepAlive include={sys.keepRoutes}>
                          <Component
                            is={route.name}
                            key={route.name}
                          ></Component>
                        </KeepAlive>
                      </Transition>
                    );
                  },
                }}
              </RouterView>
            </div>
          </div>
        </div>
      </NConfigProvider>
    );
  },
});
export default layout;
