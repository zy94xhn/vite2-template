import { toMoney } from "@utils/utils";
import { defineComponent,PropType, watch,Ref,ref,reactive, onMounted } from "vue";

const setupNumber = defineComponent({
  name:"setupNumber",
  props:{
    className:{
      type:String,
      required:false
    },
    number:{
      type:Number,
      required:true
    },
    fixed:{
      type:Number,
      required:false
    }
  },
  setup(prop,ctx) {
    const interValNumber:Ref<number> = ref<number>(0);
    let timer:any | null = null
    onMounted(()=>{
      if(prop.fixed) {
        interValNumber.value = interValNumber.value+1/(Math.pow(10,prop.fixed))
      }
      updateNumber(prop.number)
    })
    watch(()=>prop.number,(newVal)=>{
      updateNumber(newVal)
    })
    const updateNumber = (val:number)=>{
     timer = setInterval(()=>{
      if(interValNumber.value === val) {
        clearInterval(timer)
      } else {
        if(prop.fixed) {
          interValNumber.value += 1/(Math.pow(10,prop.fixed))
        }
         else {
          interValNumber.value ++
         }
      }
     },prop.fixed?10/(Math.pow(10,prop.fixed)):10)
    }
    return ()=>(
      <div class=" w-60 h-52">
        <div class="title text-center text-green-500 text-xl py-4">营销额</div>
        <div class="number-box text-3xl text-red-500 w-full text-center">
          {interValNumber.value}
        </div>
      </div>
    )
  },
})
export default setupNumber
